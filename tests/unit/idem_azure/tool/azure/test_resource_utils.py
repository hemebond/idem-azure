import pytest


def test_construct_resource_id(hub):
    resource_id = hub.tool.azure.resource_utils.construct_resource_id(
        "compute.virtual_machines",
        {
            "resource_group_name": "rg_name",
            "virtual_machine_name": "vm_name",
            "subscription_id": "s_id",
        },
    )
    assert (
        resource_id
        == "/subscriptions/s_id/resourceGroups/rg_name/providers/Microsoft.Compute/virtualMachines/vm_name"
    )


def test_construct_resource_id_missing_values(hub):
    with pytest.raises(ValueError) as excinfo:
        resource_id = hub.tool.azure.resource_utils.construct_resource_id(
            "compute.virtual_machines",
            {
                "resource_group_name": None,
                "virtual_machine_name": "vm_name",
                "subscription_id": "s_id",
            },
        )
    assert (
        str(excinfo.value)
        == f"Could not construct resource_id for compute.virtual_machines: property resource_group_name is missing or empty"
    )

    with pytest.raises(ValueError) as excinfo:
        resource_id = hub.tool.azure.resource_utils.construct_resource_id(
            "compute.virtual_machines",
            {"virtual_machine_name": "vm_name", "subscription_id": "s_id"},
        )
    assert (
        str(excinfo.value)
        == f"Could not construct resource_id for compute.virtual_machines: property resource_group_name is missing or empty"
    )

    with pytest.raises(ValueError) as excinfo:
        resource_id = hub.tool.azure.resource_utils.construct_resource_id(
            "compute.disks",
            {
                "resource_group_name": "res_grp",
                "disk_name": "   ",
                "subscription_id": "s_id",
            },
        )
    assert (
        str(excinfo.value)
        == f"Could not construct resource_id for compute.disks: property disk_name is missing or empty"
    )
