def test_basic_format_conversion(hub):
    virtual_network_peerings_present_to_raw_map = {
        "virtualNetworkPeerings": [
            {
                "$list_refs": ["virtual_network_peerings"],
                "name": "virtual_network_peerings.name",
                "type": "virtual_network_peerings.type",
                "properties": {
                    "allowVirtualNetworkAccess": "virtual_network_peerings.allow_virtual_network_access",
                    "allowForwardedTraffic": "virtual_network_peerings.allow_forwarded_traffic",
                    "allowGatewayTransit": "virtual_network_peerings.allow_gateway_transit",
                    "useRemoteGateways": "virtual_network_peerings.use_remote_gateways",
                    "remoteVirtualNetwork": "virtual_network_peerings.remote_virtual_network",
                    "remoteAddressSpace": {
                        "addressPrefixes": "virtual_network_peerings.remote_address_space.address_prefixes"
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": "virtual_network_peerings.remote_virtual_network_address_space.address_prefixes"
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": "virtual_network_peerings.remote_bgp_communities.virtual_network_community"
                    },
                    "peeringState": "virtual_network_peerings.peering_state",
                    "peeringSyncLevel": "virtual_network_peerings.peering_sync_level",
                    "doNotVerifyRemoteGateways": "virtual_network_peerings.do_not_verify_remote_gateways",
                },
            }
        ]
    }

    virtual_network_peerings_raw_to_present_map = {
        "virtual_network_peerings": [
            {
                "$list_refs": ["virtualNetworkPeerings"],
                "name": "virtualNetworkPeerings.name",
                "type": "virtualNetworkPeerings.type",
                "allow_virtual_network_access": "virtualNetworkPeerings.properties.allowVirtualNetworkAccess",
                "allow_forwarded_traffic": "virtualNetworkPeerings.properties.allowForwardedTraffic",
                "allow_gateway_transit": "virtualNetworkPeerings.properties.allowGatewayTransit",
                "use_remote_gateways": "virtualNetworkPeerings.properties.useRemoteGateways",
                "remote_virtual_network": "virtualNetworkPeerings.properties.remoteVirtualNetwork",
                "remote_address_space": {
                    "address_prefixes": "virtualNetworkPeerings.properties.remoteAddressSpace.addressPrefixes"
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": "virtualNetworkPeerings.properties.remoteVirtualNetworkAddressSpace.addressPrefixes"
                },
                "remote_bgp_communities": {
                    "virtual_network_community": "virtualNetworkPeerings.properties.remoteBgpCommunities.virtualNetworkCommunity"
                },
                "peering_state": "virtualNetworkPeerings.properties.peeringState",
                "peering_sync_level": "virtualNetworkPeerings.properties.peeringSyncLevel",
                "do_not_verify_remote_gateways": "virtualNetworkPeerings.properties.doNotVerifyRemoteGateways",
            }
        ]
    }

    present_vn_peerings_state = {
        "virtual_network_peerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "allow_forwarded_traffic": False,
                "allow_gateway_transit": True,
                "use_remote_gateways": False,
                "remote_virtual_network": True,
                "remote_address_space": {
                    "address_prefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
                "remote_bgp_communities": {
                    "virtual_network_community": ["aa", "a", "a"]
                },
                "peering_state": "fdeas",
                "peering_sync_level": "gfdg",
                "do_not_verify_remote_gateways": True,
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "allow_forwarded_traffic": False,
                "allow_gateway_transit": True,
                "use_remote_gateways": False,
                "remote_virtual_network": True,
                "remote_address_space": {
                    "address_prefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
                "remote_bgp_communities": {
                    "virtual_network_community": ["aa", "a", "a"]
                },
                "peering_state": "fdeas",
                "peering_sync_level": "gfdg",
                "do_not_verify_remote_gateways": True,
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "allow_forwarded_traffic": False,
                "allow_gateway_transit": True,
                "use_remote_gateways": False,
                "remote_virtual_network": True,
                "remote_address_space": {
                    "address_prefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
                "remote_bgp_communities": {
                    "virtual_network_community": ["aa", "a", "a"]
                },
                "peering_state": "fdeas",
                "peering_sync_level": "gfdg",
                "do_not_verify_remote_gateways": True,
            },
        ]
    }

    raw_vn_peerings_state = {
        "virtualNetworkPeerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "allowForwardedTraffic": False,
                    "allowGatewayTransit": True,
                    "useRemoteGateways": False,
                    "remoteVirtualNetwork": True,
                    "remoteAddressSpace": {
                        "addressPrefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": ["aa", "a", "a"]
                    },
                    "peeringState": "fdeas",
                    "peeringSyncLevel": "gfdg",
                    "doNotVerifyRemoteGateways": True,
                },
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "allowForwardedTraffic": False,
                    "allowGatewayTransit": True,
                    "useRemoteGateways": False,
                    "remoteVirtualNetwork": True,
                    "remoteAddressSpace": {
                        "addressPrefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": ["aa", "a", "a"]
                    },
                    "peeringState": "fdeas",
                    "peeringSyncLevel": "gfdg",
                    "doNotVerifyRemoteGateways": True,
                },
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "allowForwardedTraffic": False,
                    "allowGatewayTransit": True,
                    "useRemoteGateways": False,
                    "remoteVirtualNetwork": True,
                    "remoteAddressSpace": {
                        "addressPrefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": ["aa", "a", "a"]
                    },
                    "peeringState": "fdeas",
                    "peeringSyncLevel": "gfdg",
                    "doNotVerifyRemoteGateways": True,
                },
            },
        ]
    }

    check_format_conversion(
        hub,
        virtual_network_peerings_present_to_raw_map,
        raw_vn_peerings_state,
        virtual_network_peerings_raw_to_present_map,
        present_vn_peerings_state,
    )


def test_format_conversion_limited_state(hub):
    virtual_network_peerings_present_to_raw_map = {
        "virtualNetworkPeerings": [
            {
                "$list_refs": ["virtual_network_peerings"],
                "name": "virtual_network_peerings.name",
                "type": "virtual_network_peerings.type",
                "properties": {
                    "allowVirtualNetworkAccess": "virtual_network_peerings.allow_virtual_network_access",
                    "allowForwardedTraffic": "virtual_network_peerings.allow_forwarded_traffic",
                    "allowGatewayTransit": "virtual_network_peerings.allow_gateway_transit",
                    "useRemoteGateways": "virtual_network_peerings.use_remote_gateways",
                    "remoteVirtualNetwork": "virtual_network_peerings.remote_virtual_network",
                    "remoteAddressSpace": {
                        "addressPrefixes": "virtual_network_peerings.remote_address_space.address_prefixes"
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": "virtual_network_peerings.remote_virtual_network_address_space.address_prefixes"
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": "virtual_network_peerings.remote_bgp_communities.virtual_network_community"
                    },
                    "peeringState": "virtual_network_peerings.peering_state",
                    "peeringSyncLevel": "virtual_network_peerings.peering_sync_level",
                    "doNotVerifyRemoteGateways": "virtual_network_peerings.do_not_verify_remote_gateways",
                },
            }
        ]
    }

    virtual_network_peerings_raw_to_present_map = {
        "virtual_network_peerings": [
            {
                "$list_refs": ["virtualNetworkPeerings"],
                "name": "virtualNetworkPeerings.name",
                "type": "virtualNetworkPeerings.type",
                "allow_virtual_network_access": "virtualNetworkPeerings.properties.allowVirtualNetworkAccess",
                "allow_forwarded_traffic": "virtualNetworkPeerings.properties.allowForwardedTraffic",
                "allow_gateway_transit": "virtualNetworkPeerings.properties.allowGatewayTransit",
                "use_remote_gateways": "virtualNetworkPeerings.properties.useRemoteGateways",
                "remote_virtual_network": "virtualNetworkPeerings.properties.remoteVirtualNetwork",
                "remote_address_space": {
                    "address_prefixes": "virtualNetworkPeerings.properties.remoteAddressSpace.addressPrefixes"
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": "virtualNetworkPeerings.properties.remoteVirtualNetworkAddressSpace.addressPrefixes"
                },
                "remote_bgp_communities": {
                    "virtual_network_community": "virtualNetworkPeerings.properties.remoteBgpCommunities.virtualNetworkCommunity"
                },
                "peering_state": "virtualNetworkPeerings.properties.peeringState",
                "peering_sync_level": "virtualNetworkPeerings.properties.peeringSyncLevel",
                "do_not_verify_remote_gateways": "virtualNetworkPeerings.properties.doNotVerifyRemoteGateways",
            }
        ]
    }

    present_vn_peerings_state = {
        "virtual_network_peerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
                "remote_bgp_communities": {
                    "virtual_network_community": ["aa", "a", "a"]
                },
                "peering_state": "fdeas",
                "peering_sync_level": "gfdg",
                "do_not_verify_remote_gateways": True,
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "allow_gateway_transit": True,
                "use_remote_gateways": False,
                "remote_virtual_network": True,
                "remote_address_space": {
                    "address_prefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                },
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
                "do_not_verify_remote_gateways": True,
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "allow_virtual_network_access": True,
                "allow_forwarded_traffic": False,
                "allow_gateway_transit": True,
                "use_remote_gateways": False,
                "remote_virtual_network": True,
                "remote_address_space": {"address_prefixes": []},
                "remote_virtual_network_address_space": {
                    "address_prefixes": ["gaf", "j", "fdgd"]
                },
            },
        ]
    }

    raw_vn_peerings_state = {
        "virtualNetworkPeerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                    "remoteBgpCommunities": {
                        "virtualNetworkCommunity": ["aa", "a", "a"]
                    },
                    "peeringState": "fdeas",
                    "peeringSyncLevel": "gfdg",
                    "doNotVerifyRemoteGateways": True,
                },
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "allowGatewayTransit": True,
                    "useRemoteGateways": False,
                    "remoteVirtualNetwork": True,
                    "remoteAddressSpace": {
                        "addressPrefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                    },
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                    "doNotVerifyRemoteGateways": True,
                },
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "properties": {
                    "allowVirtualNetworkAccess": True,
                    "allowForwardedTraffic": False,
                    "allowGatewayTransit": True,
                    "useRemoteGateways": False,
                    "remoteVirtualNetwork": True,
                    "remoteAddressSpace": {"addressPrefixes": []},
                    "remoteVirtualNetworkAddressSpace": {
                        "addressPrefixes": ["gaf", "j", "fdgd"]
                    },
                },
            },
        ]
    }

    check_format_conversion(
        hub,
        virtual_network_peerings_present_to_raw_map,
        raw_vn_peerings_state,
        virtual_network_peerings_raw_to_present_map,
        present_vn_peerings_state,
    )


def test_format_conversion_lists(hub):
    virtual_network_peerings_present_to_raw_map = {
        "virtualNetworkPeerings": [
            {
                "$list_refs": ["virtual_network_peerings", "list_prop"],
                "name": "virtual_network_peerings.name",
                "type": "virtual_network_peerings.type",
                "exampleProp": {"exampleItem": "list_prop.example_item"},
                "properties": {
                    "remoteAddressSpace": {
                        "addressPrefixes": "virtual_network_peerings.remote_address_space.address_prefixes",
                        "nestedList": [
                            {
                                "$list_refs": ["list_prop.nested_list"],
                                "exampleNestedListItem": "list_prop.nested_list.example_nested_list_item",
                            }
                        ],
                    }
                },
            }
        ]
    }

    virtual_network_peerings_raw_to_present_map = {
        "list_prop": [
            {
                "$list_refs": ["virtualNetworkPeerings"],
                "example_item": "virtualNetworkPeerings.exampleProp.exampleItem",
                "nested_list": [
                    {
                        "$list_refs": [
                            "virtualNetworkPeerings.properties.remoteAddressSpace.nestedList"
                        ],
                        "example_nested_list_item": "virtualNetworkPeerings.properties.remoteAddressSpace.nestedList.exampleNestedListItem",
                    }
                ],
            }
        ],
        "virtual_network_peerings": [
            {
                "$list_refs": ["virtualNetworkPeerings"],
                "name": "virtualNetworkPeerings.name",
                "type": "virtualNetworkPeerings.type",
                "remote_address_space": {
                    "address_prefixes": "virtualNetworkPeerings.properties.remoteAddressSpace.addressPrefixes"
                },
            }
        ],
    }

    present_vn_peerings_state = {
        "list_prop": [
            {
                "example_item": 1,
                "nested_list": [
                    {"example_nested_list_item": "l1"},
                    {"example_nested_list_item": "l2"},
                ],
            },
            {
                "example_item": 2,
                "nested_list": [
                    {"example_nested_list_item": "a1"},
                    {"example_nested_list_item": "a2"},
                ],
            },
            {
                "example_item": 3,
                "nested_list": [
                    {"example_nested_list_item": "b1"},
                    {"example_nested_list_item": "b2"},
                ],
            },
        ],
        "virtual_network_peerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "remote_address_space": {"address_prefixes": ["1", "2", "3"]},
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "remote_address_space": {
                    "address_prefixes": ["afsdf", "adfsdf", "fdsffdsdf"]
                },
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "remote_address_space": {"address_prefixes": ["A"]},
            },
        ],
    }

    raw_vn_peerings_state = {
        "virtualNetworkPeerings": [
            {
                "name": "dfds",
                "type": "fdsfs",
                "exampleProp": {"exampleItem": 1},
                "properties": {
                    "remoteAddressSpace": {
                        "addressPrefixes": ["1", "2", "3"],
                        "nestedList": [
                            {"exampleNestedListItem": "l1"},
                            {"exampleNestedListItem": "l2"},
                        ],
                    },
                },
            },
            {
                "name": "dfds2",
                "type": "fdsfs",
                "exampleProp": {"exampleItem": 2},
                "properties": {
                    "remoteAddressSpace": {
                        "addressPrefixes": ["afsdf", "adfsdf", "fdsffdsdf"],
                        "nestedList": [
                            {"exampleNestedListItem": "a1"},
                            {"exampleNestedListItem": "a2"},
                        ],
                    },
                },
            },
            {
                "name": "dfds3",
                "type": "fdsfs",
                "exampleProp": {"exampleItem": 3},
                "properties": {
                    "remoteAddressSpace": {
                        "addressPrefixes": ["A"],
                        "nestedList": [
                            {"exampleNestedListItem": "b1"},
                            {"exampleNestedListItem": "b2"},
                        ],
                    },
                },
            },
        ]
    }

    check_format_conversion(
        hub,
        virtual_network_peerings_present_to_raw_map,
        raw_vn_peerings_state,
        virtual_network_peerings_raw_to_present_map,
        present_vn_peerings_state,
    )


def check_format_conversion(
    hub, present_to_raw_map, raw_state, raw_to_present_map, present_state
):
    converted_raw_state = hub.tool.azure.generic.convert_state_format(
        present_state, present_to_raw_map
    )
    assert converted_raw_state == raw_state

    converted_present_state = hub.tool.azure.generic.convert_state_format(
        raw_state, raw_to_present_map
    )
    assert converted_present_state == present_state
