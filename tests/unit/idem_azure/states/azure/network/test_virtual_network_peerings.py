from collections import ChainMap

import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils

RESOURCE_NAME = "peer"

SHORT_RESOURCE_TYPE = "network.virtual_network_peerings"
AZURE_RESOURCE_TYPE = f"azure.{SHORT_RESOURCE_TYPE}"

RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/virtualNetworkPeerings/{virtual_network_peering_name}"
RESOURCE_ID_PROPERTIES = {
    "subscription_id": "my-sub-id",
    "resource_group_name": "my-resource-group",
    "virtual_network_name": "my-vnet",
    "virtual_network_peering_name": "my-peerings",
}

RESOURCE_PARAMETERS = {
    "allow_virtual_network_access": True,
    "allow_forwarded_traffic": False,
    "allow_gateway_transit": True,
    "use_remote_gateways": False,
    "remote_virtual_network": {
        "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
    },
    "do_not_verify_remote_gateways": True,
}

RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "allowVirtualNetworkAccess": True,
        "allowForwardedTraffic": False,
        "allowGatewayTransit": True,
        "useRemoteGateways": False,
        "remoteVirtualNetwork": {
            "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
        },
        "doNotVerifyRemoteGateways": True,
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists_create(hub, ctx):
    """
    Test 'present' state of virtual network peerings. When a resource does not exist, 'present' should create the resource.
    """
    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists_update(hub, ctx):
    """
    Test 'present' state of virtual network peerings. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    resource_parameters_update_present = {
        "allow_virtual_network_access": False,
        "allow_forwarded_traffic": True,
        "allow_gateway_transit": False,
        "use_remote_gateways": True,
        "remote_virtual_network": {
            "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
        },
        "do_not_verify_remote_gateways": False,
    }

    resource_parameters_update_raw = {
        "properties": {
            "allowVirtualNetworkAccess": False,
            "allowForwardedTraffic": True,
            "allowGatewayTransit": False,
            "useRemoteGateways": True,
            "remoteVirtualNetwork": {
                "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
            },
            "doNotVerifyRemoteGateways": False,
        }
    }
    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        resource_parameters_update_raw,
        resource_parameters_update_present,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of virtual network peerings. When a resource does not exist, 'absent' should just return success.
    """
    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of virtual network peerings. When a resource exists, 'absent' should delete the resource.
    """
    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


# keep existing describe as virtual network peerings describe
# follows nonstandard describe structure
@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of virtual network peerings.
    """
    mock_hub.states.azure.network.virtual_network_peerings.describe = (
        hub.states.azure.network.virtual_network_peerings.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )

    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.exec.azure.network.virtual_network_peerings.list = (
        hub.exec.azure.network.virtual_network_peerings.list
    )

    resource_id = RESOURCE_ID_FORMAT.format(**RESOURCE_ID_PROPERTIES)
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.azure.network.virtual_networks.list.return_value = {
        "ret": [
            {
                "virtual_network_name": RESOURCE_ID_PROPERTIES["virtual_network_name"],
                "resource_group_name": RESOURCE_ID_PROPERTIES["resource_group_name"],
            }
        ],
        "result": True,
        "comment": "",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.virtual_network_peerings.describe(ctx)

    assert len(ret) == 1
    assert resource_id in ret.keys()
    ret_value = ret.get(resource_id)
    assert len(ret_value) == 1
    assert f"{AZURE_RESOURCE_TYPE}.present" in ret_value

    described_resource = ret_value.get(f"{AZURE_RESOURCE_TYPE}.present")
    described_resource_map = dict(ChainMap(*described_resource))
    expected_state = {
        **RESOURCE_PARAMETERS,
        **RESOURCE_ID_PROPERTIES,
        "resource_id": resource_id,
        "name": resource_id,
    }
    assert described_resource_map == expected_state
