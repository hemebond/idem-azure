from collections import ChainMap

import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG

RESOURCE_NAME = "my-resource"
RESOURCE_ID_TEMPLATE = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
RESOURCE_TYPE = "sql_database.databases"
RESOURCE_ID_PROPERTIES = {
    "database_name": "my-sqldb",
    "subscription_id": "my-subscription-id",
    "resource_group_name": "my-resource-group",
    "server_name": "my-server",
}
RESOURCE_PARAMETERS = {
    "location": "westeurope",
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 10,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 268435456000,
    "read_scale": "Disabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"tag-key": "tag-value"},
}
RESOURCE_PARAMETERS_RAW = {
    "name": RESOURCE_ID_PROPERTIES["database_name"],
    "location": "westeurope",
    "properties": {
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "maxSizeBytes": 268435456000,
        "readScale": "Disabled",
        "requestedBackupStorageRedundancy": "Local",
    },
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 10,
    },
    "tags": {"tag-key": "tag-value"},
}


RESOURCE_PARAMETERS_UPDATE = {
    "location": "westeurope",
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 15,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 268435456000,
    "read_scale": "Enabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"tag-key-updated": "tag-value-updated"},
}


RESOURCE_PARAMETERS_UPDATE_RAW = {
    "name": RESOURCE_ID_PROPERTIES["database_name"],
    "location": "westeurope",
    "properties": {
        "collation": "SQL_Latin1_General_CP1_CI_AS",
        "maxSizeBytes": 268435456000,
        "readScale": "Enabled",
        "requestedBackupStorageRedundancy": "Local",
    },
    "sku": {
        "name": "Standard",
        "tier": "Standard",
        "capacity": 15,
    },
    "tags": {"tag-key-updated": "tag-value-updated"},
}


@pytest.fixture(scope="module", autouse=True)
def setup_subscription_id(ctx):
    RESOURCE_ID_PROPERTIES["subscription_id"] = ctx["acct"]["subscription_id"]


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, ctx):
    """
    Test 'present' state of the resource. When a resource does not exist, 'present' should create the resource.
    """

    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, ctx):
    """
    Test 'present' state of the resource. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """

    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE_RAW,
        RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of the resource. When a resource does not exist, 'absent' should just return success.
    """

    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of the resource. When a resource exists, 'absent' should delete the resource.
    """

    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of SQL databases.
    """
    mock_hub.states.azure.sql_database.databases.describe = (
        hub.states.azure.sql_database.databases.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.sql_database.databases.convert_raw_database_to_present = (
        hub.tool.azure.sql_database.databases.convert_raw_database_to_present
    )
    mock_hub.exec.azure.sql_database.databases.list = (
        hub.exec.azure.sql_database.databases.list
    )
    mock_hub.tool.azure.resource_utils.construct_resource_id = (
        hub.tool.azure.resource_utils.construct_resource_id
    )

    resource_id = f"/subscriptions/{RESOURCE_ID_PROPERTIES['subscription_id']}/resourceGroups/{RESOURCE_ID_PROPERTIES['resource_group_name']}/providers/Microsoft.Sql/servers/{RESOURCE_ID_PROPERTIES['server_name']}/databases/{RESOURCE_ID_PROPERTIES['database_name']}"
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.sql_database.databases.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.sql_database.databases.present" in ret_value.keys()
    described_resource = ret_value.get("azure.sql_database.databases.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert RESOURCE_ID_PROPERTIES["database_name"] == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
# same behaviour with or without resource id flag
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_resource_id_not_passed_successful_create(
    hub, ctx, fake_acct_data, __test, __resource_id_flag
):
    unit_test_utils.test_resource_id_not_passed_successful_create(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub, ctx, fake_acct_data, __test
):
    unit_test_utils.test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
        hub,
        ctx,
        fake_acct_data,
        __test,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_ID_PROPERTIES["resource_group_name"] == old_state.get(
            "resource_group_name"
        )
        assert RESOURCE_ID_PROPERTIES["server_name"] == old_state.get("server_name")
        assert RESOURCE_ID_PROPERTIES["database_name"] == old_state.get("database_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert RESOURCE_ID_PROPERTIES["subscription_id"] == old_state.get(
            "subscription_id"
        )
    if new_state:
        assert RESOURCE_ID_PROPERTIES["resource_group_name"] == new_state.get(
            "resource_group_name"
        )
        assert RESOURCE_ID_PROPERTIES["server_name"] == new_state.get("server_name")
        assert RESOURCE_ID_PROPERTIES["database_name"] == new_state.get("database_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert RESOURCE_ID_PROPERTIES["subscription_id"] == new_state.get(
            "subscription_id"
        )
