from typing import Any
from typing import Dict
from typing import List

from dict_tools import data


def unmock_generic_conversion(hub, mock_hub):
    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )


def unmock_update_payload_generation(hub, mock_hub):
    mock_hub.tool.azure.generic.compute_update_payload_for_key_subset = (
        hub.tool.azure.generic.compute_update_payload_for_key_subset
    )

    mock_hub.tool.azure.generic.get_update_payload = (
        hub.tool.azure.generic.get_update_payload
    )

    mock_hub.tool.azure.utils.cleanup_none_values = (
        hub.tool.azure.utils.cleanup_none_values
    )

    mock_hub.tool.azure.compare.compare_exact_matches = (
        hub.tool.azure.compare.compare_exact_matches
    )


def unmock_exec_get_with_result(
    hub, mock_hub, resource_type: str, resource_id: str, result: Dict[str, Any]
):
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_resource_exec_ref = getattr(mock_hub.exec.azure, f"{resource_type}")
    real_resource_exec_ref = getattr(hub.exec.azure, f"{resource_type}")
    mock_resource_exec_ref.get = real_resource_exec_ref.get

    async def mock_get(_ctx, url, success_codes):
        assert resource_id in url
        return result

    mock_hub.exec.request.json.get = mock_get


def unmock_exec_get_empty_result(hub, mock_hub, resource_type: str, resource_id: str):
    expected_result = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    unmock_exec_get_with_result(
        hub, mock_hub, resource_type, resource_id, expected_result
    )


def unmock_generic_present_creation(hub, mock_hub, resource_type: str):
    mock_resource_state_ref = getattr(mock_hub.states.azure, f"{resource_type}")
    real_resource_state_ref = getattr(hub.states.azure, f"{resource_type}")
    mock_resource_state_ref.present = real_resource_state_ref.present

    mock_resource_tool_ref = getattr(mock_hub.tool.azure, f"{resource_type}")
    real_resource_tool_ref = getattr(hub.tool.azure, f"{resource_type}")
    mock_resource_tool_ref.convert_present_to_raw_state = (
        real_resource_tool_ref.convert_present_to_raw_state
    )
    mock_resource_tool_ref.convert_raw_to_present_state = (
        real_resource_tool_ref.convert_raw_to_present_state
    )

    mock_hub.tool.azure.generic.run_present = hub.tool.azure.generic.run_present
    mock_hub.tool.azure.generic.run_put_request = hub.tool.azure.generic.run_put_request
    unmock_generic_conversion(hub, mock_hub)

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    mock_hub.tool.azure.comment_utils.would_create_comment = (
        hub.tool.azure.comment_utils.would_create_comment
    )
    mock_hub.tool.azure.comment_utils.create_comment = (
        hub.tool.azure.comment_utils.create_comment
    )
    mock_hub.tool.azure.comment_utils.could_not_create_comment = (
        hub.tool.azure.comment_utils.could_not_create_comment
    )


def mock_wrapper_get_present_context(
    mock_hub,
    ctx,
    resource_type: str,
    old_state: Dict[str, Any] = None,
    name: str = None,
    resource_id: str = None,
    api_version: str = None,
):
    resource_ref = mock_hub.states.azure
    for resource_path_segment in resource_type.split("."):
        resource_ref = resource_ref[resource_path_segment]
    present_ref = resource_ref.present
    present_call_wrappers = present_ref.contract_functions.get("call")
    if present_call_wrappers:
        present_call_wrappers.pop()

    mock_wrapper_result = {
        "wrapper_result": {
            "result": True,
            "old_state": old_state,
            "new_state": None,
            "name": name,
            "comment": [],
        },
        "computed": {
            "resource_id": resource_id,
            "resource_url": f"https://management.azure.com{resource_id}?api-version={api_version}",
        },
    }
    return data.NamespaceDict(**ctx, **mock_wrapper_result)


def mock_exec_get_empty_resource(hub, expected_resource_id: str, resource_type: str):
    async def mock_get_resource(*args, resource_id: str, raw: bool = False, **kwargs):
        if expected_resource_id:
            assert resource_id == expected_resource_id
        # mimic convert output
        return dict(comment=["Not found"], result=True, ret=None)

    getattr(hub.exec.azure, resource_type).get = mock_get_resource

    def mock_get(*args, **kwargs):
        raise RuntimeError(
            f"{resource_type} get should be mocked, so this shouldn't be reached"
        )

    hub.exec.request.json.get = mock_get


def mock_exec_get_return_resource(
    hub,
    expected_resource_id: str,
    resource_id_properties,
    resource_parameters_present,
    resource_type,
):
    async def mock_get_resource(*args, resource_id: str, raw: bool = False, **kwargs):
        assert resource_id == expected_resource_id
        # calling get would return existing VM
        assert raw is False
        # mimic convert output
        ret_vm = {
            "name": expected_resource_id,
            "resource_id": expected_resource_id,
            **resource_id_properties,
            **resource_parameters_present,
        }
        return dict(comment=[""], result=True, ret=ret_vm)

    getattr(hub.exec.azure, resource_type).get = mock_get_resource

    def mock_get(*args, **kwargs):
        raise RuntimeError(
            f"{resource_type} get should be mocked, so this shouldn't be reached"
        )

    hub.exec.request.json.get = mock_get


def mock_get_empty(
    hub, ctx=None, expected_resource_id: str = None, resource_type: str = None
):
    expected_result = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    mock_get_return_result(
        hub, expected_result, ctx, expected_resource_id, resource_type
    )


def mock_get_return_resource(
    hub, resource, ctx=None, expected_resource_id: str = None, resource_type: str = None
):
    expected_result = {
        "ret": resource,
        "result": True,
        "status": 200,
        "comment": None,
    }
    mock_get_return_result(
        hub, expected_result, ctx, expected_resource_id, resource_type
    )


def mock_get_return_result(
    hub, result, ctx=None, expected_resource_id: str = None, resource_type: str = None
):
    expected_url = None
    if expected_resource_id:
        api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
        expected_url = (
            f"{ctx.acct.endpoint_url}{expected_resource_id}?api-version={api_version}"
        )

    async def mock_get(_ctx, url, *args, **kwargs):
        if expected_url:
            assert expected_url in url
        return result

    hub.exec.request.json.get = mock_get


def mock_error_put(hub, msg: str = None):
    async def mock_put(*args, **kwargs):
        raise RuntimeError(msg if msg else "Invalid call to put!")

    hub.exec.request.json.put = mock_put


def mock_error_delete(hub, msg: str = None):
    async def mock_delete(*args, **kwargs):
        raise RuntimeError(msg if msg else "Invalid call to delete!")

    hub.exec.request.raw.delete = mock_delete


def mock_successful_delete(
    hub,
    expected_return=None,
    ctx=None,
    expected_resource_id: str = None,
    resource_type: str = None,
    __test: bool = False,
):
    expected_url = None
    if expected_resource_id:
        api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
        expected_url = (
            f"{ctx.acct.endpoint_url}{expected_resource_id}?api-version={api_version}"
        )

    async def mock_delete(_ctx, url, *args, **kwargs):
        if _ctx.get("test", False) or __test:
            raise RuntimeError("Should not call delete in test mode!")
        else:
            if expected_url:
                assert url == expected_url

            expected_delete = {
                "ret": expected_return,
                "result": True,
                "status": 200,
                "comment": None,
            }

            return expected_delete

    hub.exec.request.raw.delete = mock_delete


def mock_successful_put(
    hub,
    ctx,
    expected_resource_id: str,
    resource_type: str,
    resource_parameters_raw: Dict[str, Any],
    __test: bool = False,
    update_params_raw: Dict[str, Any] = None,
):
    if update_params_raw is None:
        update_params_raw = {}

    async def mock_put(_ctx, url, success_codes, json):
        if __test or _ctx.get("test", False):
            raise RuntimeError("Should not call put in test mode!")
        else:
            expected_put = {
                "ret": {**resource_parameters_raw, **update_params_raw},
                "result": True,
                "status": 200,
                "comment": None,
            }
            api_version = hub.tool.azure.api_versions.get_api_version(resource_type)

            assert (
                url
                == f"{ctx.acct.endpoint_url}{expected_resource_id}?api-version={api_version}"
            )
            return expected_put

    hub.exec.request.json.put = mock_put


def mock_successful_list(
    hub,
    resource_list: List[Dict[str, Any]],
    expected_url: str = None,
):
    # TODO: Add paginated mock
    async def mock_list(_ctx, url, *args, **kwargs):
        if expected_url:
            assert url == expected_url

        expected_result = {
            "ret": {"value": resource_list},
            "result": True,
            "status": 200,
            "comment": None,
        }

        return expected_result

    hub.exec.request.json.get = mock_list
