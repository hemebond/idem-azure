import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest

RESOURCE_NAME = "my-resource"
POLICY_DEFINITION_NAME = "my-policy-definition"
RESOURCE_PARAMETERS = {
    "policy_type": "Custom",
    "metadata": {"version": "1.0.0", "some-key": "some-value"},
    "mode": "Indexed",
    "display_name": "test-location-policy-definition",
    "description": "Idem location policy definition for testing",
    "policy_rule": {
        "if": {"not": {"field": "location", "in": "[parameters('allowedLocations')]"}},
        "then": {"effect": "deny"},
    },
    "parameters": {
        "allowedLocations": {
            "type": "Array",
            "metadata": {
                "displayName": "Allowed locations",
                "description": "The list of locations that can be specified when deploying the resources",
                "strongType": "location",
            },
        },
    },
}

RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "policyType": "Custom",
        "metadata": {"version": "1.0.0", "some-key": "some-value"},
        "mode": "Indexed",
        "displayName": "test-location-policy-definition",
        "description": "Idem location policy definition for testing",
        "policyRule": {
            "if": {
                "not": {"field": "location", "in": "[parameters('allowedLocations')]"}
            },
            "then": {"effect": "deny"},
        },
        "parameters": {
            "allowedLocations": {
                "type": "Array",
                "metadata": {
                    "displayName": "Allowed locations",
                    "description": "The list of locations that can be specified when deploying the resources",
                    "strongType": "location",
                },
            },
        },
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy definitions. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.policy.policy_definitions.present = (
        hub.states.azure.policy.policy_definitions.present
    )
    mock_hub.exec.azure.policy.policy_definitions.get = (
        hub.exec.azure.policy.policy_definitions.get
    )
    mock_hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present = (
        hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present
    )
    mock_hub.tool.azure.policy.policy_definition.convert_present_to_raw_policy_definition = (
        hub.tool.azure.policy.policy_definition.convert_present_to_raw_policy_definition
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{POLICY_DEFINITION_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert POLICY_DEFINITION_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert POLICY_DEFINITION_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_definitions.present(
        test_ctx,
        RESOURCE_NAME,
        POLICY_DEFINITION_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.policy.policy_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.policy.policy_definitions.present(
        ctx,
        RESOURCE_NAME,
        POLICY_DEFINITION_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.policy.policy_definitions '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy definitions. When a resource exists, 'present' should update the resource with put
     parameters.
    """
    mock_hub.states.azure.policy.policy_definitions.present = (
        hub.states.azure.policy.policy_definitions.present
    )
    mock_hub.exec.azure.policy.policy_definitions.get = (
        hub.exec.azure.policy.policy_definitions.get
    )
    mock_hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present = (
        hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present
    )
    mock_hub.tool.azure.policy.policy_definition.update_policy_definition_payload = (
        hub.tool.azure.policy.policy_definition.update_policy_definition_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "policy_type": "Custom",
        "metadata": {"version": "2.0.0", "some-key-updated": "some-value-updated"},
        "mode": "Indexed",
        "display_name": "test-location-updated-policy-definition",
        "description": "Idem location policy definition updated for testing",
        "policy_rule": {
            "if": {
                "not": {"field": "location", "in": "[parameters('allowedLocations')]"}
            },
            "then": {"effect": "deny"},
        },
        "parameters": {
            "allowedLocations": {
                "type": "Array",
                "metadata": {
                    "displayName": "Allowed locations",
                    "description": "The list of locations that can be specified when deploying the resources",
                    "strongType": "location",
                },
            },
        },
    }

    resource_parameters_update_raw = {
        "properties": {
            "policyType": "Custom",
            "metadata": {"version": "2.0.0", "some-key-updated": "some-value-updated"},
            "mode": "Indexed",
            "displayName": "test-location-updated-policy-definition",
            "description": "Idem location policy definition updated for testing",
            "policyRule": {
                "if": {
                    "not": {
                        "field": "location",
                        "in": "[parameters('allowedLocations')]",
                    }
                },
                "then": {"effect": "deny"},
            },
            "parameters": {
                "allowedLocations": {
                    "type": "Array",
                    "metadata": {
                        "displayName": "Allowed locations",
                        "description": "The list of locations that can be specified when deploying the resources",
                        "strongType": "location",
                    },
                },
            },
        }
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{POLICY_DEFINITION_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{POLICY_DEFINITION_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert POLICY_DEFINITION_NAME in url
        assert resource_parameters_update_raw["properties"]["parameters"] == json.get(
            "properties"
        ).get("parameters")
        assert resource_parameters_update_raw["properties"]["policyType"] == json.get(
            "properties"
        ).get("policyType")
        assert resource_parameters_update_raw["properties"]["metadata"] == json.get(
            "properties"
        ).get("metadata")
        assert resource_parameters_update_raw["properties"]["mode"] == json.get(
            "properties"
        ).get("mode")
        assert resource_parameters_update_raw["properties"]["displayName"] == json.get(
            "properties"
        ).get("displayName")
        assert resource_parameters_update_raw["properties"]["description"] == json.get(
            "properties"
        ).get("description")
        assert resource_parameters_update_raw["properties"]["policyRule"] == json.get(
            "properties"
        ).get("policyRule")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_definitions.present(
        test_ctx,
        RESOURCE_NAME,
        POLICY_DEFINITION_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.policy.policy_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.policy.policy_definitions.present(
        ctx,
        RESOURCE_NAME,
        POLICY_DEFINITION_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy definitions. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.policy.policy_definitions.absent = (
        hub.states.azure.policy.policy_definitions.absent
    )
    mock_hub.exec.azure.policy.policy_definitions.get = (
        hub.exec.azure.policy.policy_definitions.get
    )
    mock_hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present = (
        hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert POLICY_DEFINITION_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.policy.policy_definitions.absent(
        ctx, RESOURCE_NAME, POLICY_DEFINITION_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.policy.policy_definitions '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy definitions. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.policy.policy_definitions.absent = (
        hub.states.azure.policy.policy_definitions.absent
    )
    mock_hub.exec.azure.policy.policy_definitions.get = (
        hub.exec.azure.policy.policy_definitions.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present = (
        hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{POLICY_DEFINITION_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert POLICY_DEFINITION_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.policy.policy_definitions.absent(
        test_ctx, RESOURCE_NAME, POLICY_DEFINITION_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.policy.policy_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.policy.policy_definitions.absent(
        ctx, RESOURCE_NAME, POLICY_DEFINITION_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.policy.policy_definitions '{RESOURCE_NAME}'" in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of policy definitions.
    """
    mock_hub.states.azure.policy.policy_definitions.describe = (
        hub.states.azure.policy.policy_definitions.describe
    )
    mock_hub.exec.azure.policy.policy_definitions.list = (
        hub.exec.azure.policy.policy_definitions.list
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present = (
        hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present
    )

    resource_id = f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{POLICY_DEFINITION_NAME}"
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.policy.policy_definitions.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.policy.policy_definitions.present" in ret_value.keys()
    described_resource = ret_value.get("azure.policy.policy_definitions.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert POLICY_DEFINITION_NAME == old_state.get("policy_definition_name")
        assert expected_old_state["policy_type"] == old_state.get("policy_type")
        assert expected_old_state["metadata"] == old_state.get("metadata")
        assert expected_old_state["mode"] == old_state.get("mode")
        assert expected_old_state["display_name"] == old_state.get("display_name")
        assert expected_old_state["description"] == old_state.get("description")
        assert expected_old_state["policy_rule"] == old_state.get("policy_rule")
        assert expected_old_state["parameters"] == old_state.get("parameters")
    if new_state:
        assert POLICY_DEFINITION_NAME == new_state.get("policy_definition_name")
        assert expected_new_state["policy_type"] == new_state.get("policy_type")
        assert expected_new_state["metadata"] == new_state.get("metadata")
        assert expected_new_state["mode"] == new_state.get("mode")
        assert expected_new_state["display_name"] == new_state.get("display_name")
        assert expected_new_state["description"] == new_state.get("description")
        assert expected_new_state["policy_rule"] == new_state.get("policy_rule")
        assert expected_new_state["parameters"] == new_state.get("parameters")
