import sys
import unittest.mock as mock
from typing import Any
from typing import Callable
from typing import Dict

import pop.hub
import pytest
from dict_tools import data
from pytest_idem import runner


@pytest.fixture(scope="function", name="hub")
def unit_hub(code_dir):
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        for dyne in ["idem", "pop_create"]:
            hub.pop.sub.add(dyne_name=dyne)
        hub.pop.config.load(
            ["idem", "pop_create", "acct", "rend"], cli="idem", parse_cli=False
        )
        hub.idem.RUNS = {"test": {}}
        hub.tool.azure.test_utils.fake_login_on_hub()
        yield hub


@pytest.fixture(scope="module")
def fake_acct_data():
    fake_subscription = "12345678-1234-1234-1234-aaabc1234aaa"
    fake_client_id = "76543210-4321-4321-4321-bbbb3333aaaa"
    fake_secret = "ZzxxxXXXX11xx-aaaaabbbb-k3xxxxxx"
    tenant = "bbbbbca-3333-4444-aaaa-cddddddd6666"
    return {
        "profiles": {
            "azure": {
                "default": {
                    "endpoint_url": "https://management.azure.com",
                    "client_id": fake_client_id,
                    "secret": fake_secret,
                    "subscription_id": fake_subscription,
                    "tenant": tenant,
                    "headers": dict(),
                }
            }
        }
    }


@pytest.fixture(scope="module")
def ctx(fake_acct_data: Dict[str, Any]):
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        acct=data.NamespaceDict(**fake_acct_data["profiles"]["azure"]["default"]),
    )


@pytest.fixture(scope="module")
def idem_cli(fake_acct_data: Dict[str, Any]) -> Callable:
    return lambda *args, **kwargs: runner.idem_cli(
        *args, acct_data=fake_acct_data, **kwargs
    )
