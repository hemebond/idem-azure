"""
Integration tests for rest.py
"""


def test_context(hub):
    ...


def test_parse(hub, url, desc):
    """
    Test the parse function for completion and expected return.
    :param hub:
    :param url:
    :return:
    """
    operation_groups = hub.pop_create.azure.rest.parse(url, desc)
    operation_group_items = operation_groups.get("operation-groups")
    assert operation_group_items, "No operation groups retrieved from api"
    managed_clusters = next(
        (
            og
            for og in operation_group_items
            if og.get("resource") == "managed-clusters"
        ),
        None,
    )
    assert {
        "resource": "managed-clusters",
        "create": "https://learn.microsoft.com/en-us/rest/api/aks/managed-clusters/create-or-update",
        "delete": "https://learn.microsoft.com/en-us/rest/api/aks/managed-clusters/delete",
        "get": "https://learn.microsoft.com/en-us/rest/api/aks/managed-clusters/get",
        "list": "https://learn.microsoft.com/en-us/rest/api/aks/managed-clusters/list",
        "update": "https://learn.microsoft.com/en-us/rest/api/aks/managed-clusters/update-tags",
    } == managed_clusters
