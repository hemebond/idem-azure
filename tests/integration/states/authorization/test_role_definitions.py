import asyncio
import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_role_definitions(hub, ctx):
    """
    This test provisions a role_definition, describes role_definitions and deletes the provisioned role_definition.
    """
    # Create role definition
    scope = f"/subscriptions/{ctx.acct.subscription_id}"
    role_definition_id = str(uuid.uuid4())
    role_definition_name = "role_definition-" + str(uuid.uuid4())
    role_definition_parameters = {
        "role_definition_name": role_definition_name,
        "description": "Custom role created for test",
        "assignable_scopes": ["/subscriptions/3d1a2f6d-86de-4f00-bfa5-ad022e400894"],
        "permissions": [
            {
                "actions": ["Microsoft.Resources/subscriptions/resourceGroups/read"],
                "notActions": [
                    "Microsoft.Resources/subscriptions/resourceGroups/write"
                ],
                "dataActions": [
                    "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read"
                ],
                "notDataActions": [
                    "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write"
                ],
            }
        ],
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create role_definition with --test
    role_definition_ret = await hub.states.azure.authorization.role_definitions.present(
        test_ctx,
        name=role_definition_name,
        role_definition_id=role_definition_id,
        scope=scope,
        **role_definition_parameters,
    )
    assert role_definition_ret["result"], role_definition_ret["comment"]
    assert not role_definition_ret["old_state"] and role_definition_ret["new_state"]
    assert (
        f"Would create azure.authorization.role_definitions '{role_definition_name}'"
        in role_definition_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=role_definition_ret["new_state"],
        expected_old_state=None,
        expected_new_state=role_definition_parameters,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=role_definition_name,
    )
    resource_id = role_definition_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}"
        == resource_id
    )

    # Create role_definition in real
    role_definition_ret = await hub.states.azure.authorization.role_definitions.present(
        ctx,
        name=role_definition_name,
        role_definition_id=role_definition_id,
        scope=scope,
        **role_definition_parameters,
    )
    assert role_definition_ret["result"], role_definition_ret["comment"]
    assert not role_definition_ret["old_state"] and role_definition_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=role_definition_ret["new_state"],
        expected_old_state=None,
        expected_new_state=role_definition_parameters,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=role_definition_name,
    )
    resource_id = role_definition_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}?api-version=2022-04-01",
        retry_count=10,
        retry_period=10,
    )
    # TODO: This practice is not recommended, and we will come back and improve this test.
    # The GET API for role definitions takes time to become consistent
    # Describe role_definition
    describe_ret = await wait_for_describe_ret(
        hub, ctx, resource_id=resource_id, retry_count=20, retry_period=20
    )
    assert resource_id in describe_ret

    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.authorization.role_definitions.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=resource_id,
    )

    updated_role_definition_name = f"{role_definition_name}-updated"
    role_definition_update_parameters = {
        "role_definition_name": updated_role_definition_name,
        "description": "Custom role created for test updated",
        "assignable_scopes": [
            "/subscriptions/3d1a2f6d-86de-4f00-bfa5-ad022e400894/resourceGroups/taskforce-resource-group"
        ],
        "permissions": [
            {
                "actions": ["Microsoft.Resources/subscriptions/resourceGroups/write"],
                "notActions": ["Microsoft.Resources/subscriptions/resourceGroups/read"],
                "dataActions": [
                    "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write"
                ],
                "notDataActions": [
                    "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read"
                ],
            }
        ],
    }

    # update role_definition with --test
    role_definition_ret = await hub.states.azure.authorization.role_definitions.present(
        test_ctx,
        name=role_definition_name,
        scope=scope,
        role_definition_id=role_definition_id,
        **role_definition_update_parameters,
    )
    assert role_definition_ret["result"], role_definition_ret["comment"]
    assert role_definition_ret["old_state"] and role_definition_ret["new_state"]
    assert (
        f"Would update azure.authorization.role_definitions '{role_definition_name}'"
        in role_definition_ret["comment"]
    )
    check_returned_states(
        old_state=role_definition_ret["old_state"],
        new_state=role_definition_ret["new_state"],
        expected_old_state=role_definition_parameters,
        expected_new_state=role_definition_update_parameters,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=role_definition_name,
    )
    resource_id = role_definition_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}"
        == resource_id
    )

    # Update role definition in real
    role_definition_ret = await hub.states.azure.authorization.role_definitions.present(
        ctx,
        name=role_definition_name,
        scope=scope,
        role_definition_id=role_definition_id,
        **role_definition_update_parameters,
    )
    assert role_definition_ret["result"], role_definition_ret["comment"]
    assert role_definition_ret["old_state"] and role_definition_ret["new_state"]
    assert (
        f"Updated azure.authorization.role_definitions '{role_definition_name}'"
        in role_definition_ret["comment"]
    )
    check_returned_states(
        old_state=role_definition_ret["old_state"],
        new_state=role_definition_ret["new_state"],
        expected_old_state=role_definition_parameters,
        expected_new_state=role_definition_update_parameters,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=role_definition_name,
    )
    resource_id = role_definition_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}"
        == resource_id
    )

    # TODO: This practice is not recommended, and we will come back and improve this test.
    # The GET API for role definitions takes time to become consistent
    await asyncio.sleep(100)
    await wait_for_updated_present(
        hub,
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-04-01",
        role_definition_name=updated_role_definition_name,
        retry_count=10,
        retry_period=10,
    )

    # Delete role_definition with --test
    role_definition_del_ret = (
        await hub.states.azure.authorization.role_definitions.absent(
            test_ctx,
            name=updated_role_definition_name,
            scope=scope,
            role_definition_id=role_definition_id,
        )
    )
    assert role_definition_del_ret["result"], role_definition_del_ret["comment"]
    assert (
        role_definition_del_ret["old_state"]
        and not role_definition_del_ret["new_state"]
    )
    assert (
        f"Would delete azure.authorization.role_definitions '{updated_role_definition_name}'"
        in role_definition_del_ret["comment"]
    )

    check_returned_states(
        old_state=role_definition_del_ret["old_state"],
        new_state=None,
        expected_old_state=role_definition_update_parameters,
        expected_new_state=None,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=updated_role_definition_name,
    )
    # Delete role_definition in real
    role_definition_del_ret = (
        await hub.states.azure.authorization.role_definitions.absent(
            ctx,
            name=updated_role_definition_name,
            scope=scope,
            role_definition_id=role_definition_id,
        )
    )

    assert role_definition_del_ret["result"], role_definition_del_ret["comment"]
    assert (
        role_definition_del_ret["old_state"]
        and not role_definition_del_ret["new_state"]
    )
    assert (
        f"Deleted azure.authorization.role_definitions '{updated_role_definition_name}'"
        in role_definition_del_ret["comment"]
    )
    check_returned_states(
        old_state=role_definition_del_ret["old_state"],
        new_state=None,
        expected_old_state=role_definition_update_parameters,
        expected_new_state=None,
        scope=scope,
        role_definition_id=role_definition_id,
        idem_resource_name=updated_role_definition_name,
    )

    # TODO: This practice is not recommended, and we will come back and improve this test.
    # The GET API for role definitions takes time to become consistent
    await asyncio.sleep(30)
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}?api-version=2022-04-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete role_definition again
    role_definition_del_ret = (
        await hub.states.azure.authorization.role_definitions.absent(
            ctx,
            name=updated_role_definition_name,
            scope=scope,
            role_definition_id=role_definition_id,
        )
    )
    assert role_definition_del_ret["result"], role_definition_del_ret["comment"]
    assert (
        not role_definition_del_ret["old_state"]
        and not role_definition_del_ret["new_state"]
    )
    assert (
        f"azure.authorization.role_definitions '{updated_role_definition_name}' already absent"
        in role_definition_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    scope,
    role_definition_id,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert scope == old_state.get("scope")
        assert role_definition_id == old_state.get("role_definition_id")
        assert expected_old_state["role_definition_name"] == old_state.get(
            "role_definition_name"
        )
        assert expected_old_state["permissions"] == old_state.get("permissions")
        assert expected_old_state["description"] == old_state.get("description")
        assert expected_old_state["assignable_scopes"] == old_state.get(
            "assignable_scopes"
        )

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert scope == new_state.get("scope")
        assert role_definition_id == new_state.get("role_definition_id")
        assert expected_new_state["role_definition_name"] == new_state.get(
            "role_definition_name"
        )
        assert expected_new_state["permissions"] == new_state.get("permissions")
        assert expected_new_state["description"] == new_state.get("description")
        assert expected_new_state["assignable_scopes"] == new_state.get(
            "assignable_scopes"
        )


async def wait_for_describe_ret(
    hub,
    ctx,
    resource_id: str,
    retry_count: int,
    retry_period: int,
):
    """
    Wait for the created resource to get reflected in describe list call
    resources that depend on each other.
    :param hub: The redistributed pop central hub. This is required in Idem, so while not used, must appear.
    :param ctx: Idem ctx.
    :param resource_id: management group name to be found on describe
    :param retry_count: The maximum number of retries to do.
    :param retry_period: The waiting time between each retry.
    """
    count = 0
    while count < retry_count:
        describe_ret = await hub.states.azure.authorization.role_definitions.describe(
            ctx
        )
        if resource_id in describe_ret:
            return describe_ret
        else:
            count += 1
            hub.log.info(f"{count}/{retry_count} Wait for resource")
            await asyncio.sleep(retry_period)

    raise RuntimeError(
        f"Resource with get did not reach 'Succeeded' state within time limit."
    )


async def wait_for_updated_present(
    hub,
    ctx,
    url: str,
    role_definition_name: str,
    retry_count: int,
    retry_period: int,
    retry_policy: list = [404],
):
    """
    Wait for the created resource to get reflected in describe list call
    resources that depend on each other.
    :param hub: The redistributed pop central hub. This is required in Idem, so while not used, must appear.
    :param ctx: Idem ctx.
    :param role_definition_id: management group name to be found on describe
    :param url: The full url to get the resource from Azure.
    :param retry_count: The maximum number of retries to do.
    :param retry_period: The waiting time between each retry.
    :param retry_policy: A list of HTTP status code on which a retry should happen.
    """
    count = 0
    while count < retry_count:
        response_get = await hub.exec.request.json.get(
            ctx,
            url=url,
            success_codes=[200],
            headers=ctx.acct.headers,
        )
        if response_get["result"]:
            ret = response_get["ret"]
            if "properties" in ret and "roleName" in ret.get("properties"):
                if ret.get("properties").get("roleName") == role_definition_name:
                    return response_get
            else:
                count += 1
                hub.log.info(f"{count}/{retry_count} Wait for resource with url {url}")
                await asyncio.sleep(retry_period)
        elif response_get["status"] in retry_policy:
            count += 1
            hub.log.info(f"{count}/{retry_count} Wait for resource with url {url}")
            await asyncio.sleep(retry_period)
        else:
            raise RuntimeError(
                f"Getting resource with url {url} failed with status {response_get['status']}"
                f" and error: {response_get['ret']}"
            )
    raise RuntimeError(
        f"Resource with url {url} did not reach 'Succeeded' state within time limit."
    )
