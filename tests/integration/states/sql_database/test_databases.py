import copy
from collections import ChainMap

import pytest

from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG

RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Sql/servers/{server_name}/databases/{database_name}"
RESOURCE_TYPE = "sql_database.databases"
PROPERTIES = {
    "location": "westeurope",
    "sku": {
        "name": "GP_Gen5",
        "tier": "GeneralPurpose",
        "family": "Gen5",
        "capacity": 2,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 17179869184,
    "read_scale": "Disabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"abc": "abcde"},
}

UPDATED_PROPERTIES = {
    "location": "westeurope",
    "sku": {
        "name": "GP_Gen5",
        "tier": "GeneralPurpose",
        "family": "Gen5",
        "capacity": 2,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 34359738368,
    "read_scale": "Disabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"org": "hello", "abc": "12345", "test": "test"},
}

UPDATED_PROPERTIES_SECOND_UPDATE = {
    "location": "westeurope",
    "sku": {
        "name": "GP_Gen5",
        "tier": "GeneralPurpose",
        "family": "Gen5",
        "capacity": 2,
    },
    "collation": "SQL_Latin1_General_CP1_CI_AS",
    "max_size_bytes": 34359738368,
    "read_scale": "Disabled",
    "requested_backup_storage_redundancy": "Local",
    "tags": {"org": "hello", "abc": "12345", "test": "test", "test1": "test1"},
}


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_create")
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
async def test_create(hub, ctx, idem_cli, __test, module_cleaner):
    name = hub.tool.azure.test_utils.generate_unique_name("idem-test-database")
    PROPERTIES["name"] = name
    PROPERTIES["database_name"] = name
    PROPERTIES["server_name"] = "parallel-preallocate-test"
    PROPERTIES["subscription_id"] = ctx.acct.get("subscription_id")
    PROPERTIES["resource_group_name"] = "default-rg"

    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )
    module_cleaner.mark_for_deletion(ret["new_state"], RESOURCE_TYPE)

    assert ret["result"], ret["comment"]
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    expected_state = {"resource_id": resource_id, **PROPERTIES}

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=expected_state,
        name=name,
        resource_id=resource_id,
    )

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_empty_update_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, __test
):
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    assert not ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.already_exists_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    expected_state = {
        "resource_id": resource_id,
        **PROPERTIES,
    }
    # TODO: this differs from idem-gcp where new_state and old_state are None
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=expected_state,
        expected_new_state=expected_state,
        name=expected_state["name"],
        resource_id=resource_id,
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
@pytest.mark.dependency(
    name="try_update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_try_update_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, __test
):
    state_to_enforce = copy.deepcopy(PROPERTIES)
    state_to_enforce.update(UPDATED_PROPERTIES)
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    # TODO: this differs from idem-gcp
    assert not ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.already_exists_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )

    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    expected_state = {
        "resource_id": resource_id,
        **PROPERTIES,
    }
    # TODO: this differs from idem-gcp where new_state and old_state are None
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=expected_state,
        expected_new_state=expected_state,
        name=expected_state["name"],
        resource_id=resource_id,
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_create"])
async def test_describe(hub, ctx):
    ret = await hub.states.azure.sql_database.databases.describe(ctx)
    current_resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    for resource_id in ret:
        described_resource = ret[resource_id].get(f"azure.{RESOURCE_TYPE}.present")
        assert described_resource
        expected_resource_state = dict(ChainMap(*described_resource))

        if resource_id == current_resource_id:
            described_resource_map = dict(ChainMap(*described_resource))
            hub.tool.azure.test_utils.check_returned_states(
                actual_state=described_resource_map,
                expected_state=expected_resource_state,
                name=PROPERTIES["database_name"],
                resource_id=current_resource_id,
            )
        else:
            assert expected_resource_state.get("resource_id") == resource_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
async def test_empty_update(hub, ctx, idem_cli, __test, __resource_id_flag):
    state_to_enforce = {
        "resource_id": RESOURCE_ID_FORMAT.format(**PROPERTIES),
        **PROPERTIES,
    }
    additional_kwargs = (
        ["--get-resource-only-with-resource-id"] if __resource_id_flag else None
    )
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, state_to_enforce, __test, additional_kwargs
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=state_to_enforce,
        expected_new_state=state_to_enforce,
        name=state_to_enforce["name"],
        resource_id=state_to_enforce["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
async def test_update(hub, ctx, idem_cli, __test):
    updated_state = copy.deepcopy(PROPERTIES)
    updated_state.update(UPDATED_PROPERTIES)
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, updated_state, __test
    )

    assert ret["result"], ret["comment"]
    resource_id = RESOURCE_ID_FORMAT.format(**updated_state)
    expected_old_state = {"resource_id": resource_id, **PROPERTIES}
    expected_new_state = {"resource_id": resource_id, **updated_state}

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=expected_old_state,
        expected_new_state=expected_new_state,
        name=updated_state["name"],
        resource_id=resource_id,
    )
    if __test:
        assert [
            hub.tool.azure.comment_utils.would_update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
@pytest.mark.dependency(
    name="test_present_update_get_resource_only_with_resource_id_with_resource_id",
    depends=["present_update"],
)
async def test_present_update_get_resource_only_with_resource_id_with_resource_id(
    hub, ctx, idem_cli, __test
):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    state_to_enforce = {"resource_id": resource_id, **PROPERTIES}
    state_to_enforce.update(UPDATED_PROPERTIES_SECOND_UPDATE)
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
            in ret["comment"]
        )

    expected_old_state = {
        "resource_id": resource_id,
        **PROPERTIES,
        **UPDATED_PROPERTIES,
    }

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=expected_old_state,
        expected_new_state=state_to_enforce,
        name=state_to_enforce["name"],
        resource_id=state_to_enforce["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="absent",
    depends=["test_present_update_get_resource_only_with_resource_id_with_resource_id"],
)
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
async def test_absent(hub, ctx, idem_cli, __test, module_cleaner):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx, idem_cli, RESOURCE_TYPE, PROPERTIES["name"], resource_id, test=__test
    )

    assert ret["result"], ret["comment"]
    current_state = copy.deepcopy(PROPERTIES)
    current_state.update(UPDATED_PROPERTIES_SECOND_UPDATE)
    expected_old_state = {"resource_id": resource_id, **current_state}

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=expected_old_state,
        expected_new_state=None,
        name=expected_old_state["name"],
        resource_id=expected_old_state["resource_id"],
    )

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
            in ret["comment"]
        )
        ret = await hub.exec.azure.sql_database.databases.get(
            ctx, resource_id=resource_id
        )
        assert ret["result"]
        assert not ret["ret"]

        module_cleaner.mark_deleted(resource_id)


@pytest.mark.asyncio
@pytest.mark.dependency(name="already_absent", depends=["absent"])
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
async def test_already_absent(hub, ctx, idem_cli, __test):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES["name"],
        resource_id,
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]

    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    ret = await hub.exec.azure.sql_database.databases.get(ctx, resource_id=resource_id)
    assert ret["result"]
    assert not ret["ret"]
